from django import forms
from django.conf import settings
from django.template.loader import render_to_string
from django.contrib.staticfiles.templatetags.staticfiles import static
from wagtail.wagtailcore.blocks import (
    ListBlock,
    StructBlock,
    CharBlock,
    RichTextBlock
)


class TabBlock(StructBlock):
    title = CharBlock()
    content = RichTextBlock()

    # class Meta:
    #     form_classname = 'tab'
    #     form_template = 'wagtailblocks_tabs/block_forms/tab.html'


class TabsBlock(ListBlock):
    def __init__(self, child_block=TabBlock(), **kwargs):
        super(TabsBlock, self).__init__(child_block, **kwargs)

    @property
    def media(self):
        return forms.Media(
            js=[
                static('wagtailadmin/js/blocks/sequence.js'), static('wagtailadmin/js/blocks/list.js'),
                static('wagtailblocks_tabs/js/tabs.js')
            ]
        )

    def js_initializer(self):
        opts = {'definitionPrefix': "'%s'" % self.definition_prefix}

        if self.child_js_initializer:
            opts['childInitializer'] = self.child_js_initializer

        return "ListBlock(%s)" % js_dict(opts)

    def render_list_member(self, value, prefix, index, errors=None):
        """
        Render the HTML for a single list item in the form. This consists of an <li> wrapper, hidden fields
        to manage ID/deleted state, delete/reorder buttons, and the child block's own form HTML.
        """
        child = self.child_block.bind(value, prefix="%s-value" % prefix, errors=errors)
        return render_to_string('wagtailblocks_tabs/block_forms/tab.html', {
            'prefix': prefix,
            'child': child,
            'index': index,
        })

    def render_form(self, value, prefix='', errors=None):
        if errors:
            if len(errors) > 1:
                raise TypeError('TabsBlock.render_form unexpectedly received multiple errors')
            error_list = errors.as_data()[0].params
        else:
            error_list = None

        list_members_html = [
            self.render_list_member(child_val, "%s-%d" % (prefix, i), i,
                                    errors=error_list[i] if error_list else None)
            for (i, child_val) in enumerate(value)
        ]

        return render_to_string('wagtailblocks_tabs/block_forms/tabs.html', {
            'help_text': getattr(self.meta, 'help_text', None),
            'prefix': prefix,
            'list_members_html': list_members_html,
        })

    class Meta:
        if 'wagtailfontawesome' in settings.INSTALLED_APPS:
            icon = 'fa-folder'
