from django.utils.html import format_html, format_html_join
from django.conf import settings

from wagtail.wagtailcore import hooks


@hooks.register('insert_editor_css')
def tabs_block_styles():
    return format_html(
        '<link rel="stylesheet" href="'
        + settings.STATIC_URL
        + 'wagtailblocks_tabs/css/wagtailblocks_tabs.css">'
    )


# @hooks.register('insert_editor_js')
# def tabs_block_scripts():
#     js_files = [
#         'wagtailblocks_tabs/js/tabs.js',
#     ]
#     js_includes = format_html_join(
#         '\n', '<script src="{0}{1}"></script>',
#         ((settings.STATIC_URL, filename) for filename in js_files)
#     )
#     return js_includes + format_html(
#         """
#         <script>
#           $( document ).ready(tabs_block_init());
#         </script>
#         """
#     )
